package co.simplon.p18.projetblog.repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.p18.projetblog.entity.Article;

@Repository
public class ArticleRepository {
    @Autowired
    private DataSource dataSource;

    public List<Article> findAll() {
        List<Article> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM article");

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Article article = new Article(
                        rs.getInt("id"),
                        rs.getString("label"),
                        rs.getDate("date").toLocalDate(),
                        rs.getString("texte"),
                        rs.getString("image")
                        );

                list.add(article);
            }
        } catch (SQLException e) {

            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }

        return list;
    }

    public Article findById(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM article WHERE id=?");

            stmt.setInt(1, id);

            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                Article article = new Article(
                        rs.getInt("id"),
                        rs.getString("label"),
                        rs.getDate("date").toLocalDate(),
                        rs.getString("texte"),
                        rs.getString("image")
                        );
                return article;

            }

        } catch (SQLException e) {

            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }

        return null;
    }

    public void save(Article article) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement
            ("INSERT INTO article (label, date, texte, image) VALUES (?,?,?,?)", PreparedStatement.RETURN_GENERATED_KEYS);

            stmt.setString(1, article.getLabel());
            stmt.setDate(2, Date.valueOf(article.getDate()));
            stmt.setString(3, article.getText());
            stmt.setString(4, article.getImage());

            stmt.executeUpdate();

            ResultSet rs = stmt.getGeneratedKeys();
            if(rs.next()) {
                article.setId(rs.getInt(1));
            }
         
        } catch (SQLException e) {
            
            e.printStackTrace();
            throw new RuntimeException("Database access error");
        }

    }


    public boolean update(Article article) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("UPDATE article SET label = ?,date = ?,texte = ?, image = ? WHERE id=?");
            
            stmt.setString(1, article.getLabel());
            stmt.setDate(2, Date.valueOf(article.getDate()));
            stmt.setString(3, article.getText());
            stmt.setString(4, article.getImage());
            stmt.setInt(5, article.getId());
            return stmt.executeUpdate() == 1;
            
    
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Database access error");

        }
    
    
    }




    public boolean deleteById(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM article WHERE id=?");
    
            stmt.setInt(1, id);
    
            return stmt.executeUpdate() == 1;
    
        } catch (SQLException e) {
            e.printStackTrace();
    
        }
        return false;
        }
    
}
