package co.simplon.p18.projetblog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetblogApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetblogApplication.class, args);
	}

}
