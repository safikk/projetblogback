package co.simplon.p18.projetblog.entity;

import java.time.LocalDate;

public class CommentsEntity {
    private String text;
    private LocalDate date;
    public CommentsEntity(String text, LocalDate date) {
        this.text = text;
        this.date = date;
    }
    public CommentsEntity() {
    }
    public String getText() {
        return text;
    }
    public void setText(String text) {
        this.text = text;
    }
    public LocalDate getDate() {
        return date;
    }
    public void setDate(LocalDate date) {
        this.date = date;
    }

}
