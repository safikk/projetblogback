package co.simplon.p18.projetblog.entity;

import java.time.LocalDate;

public class Article {
    private Integer id;
    private String label;
    private LocalDate date;
    private String text;
    private String image;
    public Article() {
    }
    public Article(String label, LocalDate date, String text, String image) {
        this.label = label;
        this.date = date;
        this.text = text;
        this.image = image;
    }
    public Article(Integer id, String label, LocalDate date, String text, String image) {
        this.id = id;
        this.label = label;
        this.date = date;
        this.text = text;
        this.image = image;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }
    public LocalDate getDate() {
        return date;
    }
    public void setDate(LocalDate date) {
        this.date = date;
    }
    public String getText() {
        return text;
    }
    public void setText(String text) {
        this.text = text;
    }
    public String getImage() {
        return image;
    }
    public void setImage(String image) {
        this.image = image;
    }

    
}
