package co.simplon.p18.projetblog.controller;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.p18.projetblog.entity.Article;
import co.simplon.p18.projetblog.repository.ArticleRepository;


@RestController
@RequestMapping("/api/article")  //Indique un préfixe qui s'appliquera à tous les Mapping du contrôleur actuel
public class ArticleController {
    @Autowired
    private ArticleRepository repo;
    
    @GetMapping
    public List<Article> all() {
        return repo.findAll();
    }


    @GetMapping("/{id}")
    public Article findById(@PathVariable int id) {
        Article article = repo.findById(id);
        if(article == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return article;
    }


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Article post(@RequestBody Article article) {
        if(article.getDate() == null) {
            article.setDate(LocalDate.now());
        }
        repo.save(article);

        return article;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable int id) {
        if(!repo.deleteById(id)){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PutMapping("/{id}")
    public Article update(@RequestBody Article article, @PathVariable int id) {
        if (id != article.getId()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
        if(!repo.update(article)){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return repo.findById(article.getId());
    }




    

}
