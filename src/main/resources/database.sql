DROP TABLE IF EXISTS article;

CREATE TABLE article(  
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
    label VARCHAR(128),
    date DATE NOT NULL,
    texte TEXT,
    image VARCHAR(128)
) ;

INSERT INTO article (label, date, texte, image) VALUES ("titre", "2022-05-03", "texte", "image"),
("premier titre", "2022-03-04", "premier texte", "image1"),
("deuxième titre", "2022-01-05", "deuxième texte", "image2"),
("troisième titre", "2022-08-06", "troisième texte", "image3");



DROP TABLE IF EXISTS comments;

CREATE TABLE comments(
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
    texte TEXT,
    date DATE NOT NULL,
) DEFAULT CHARSET UFT8;

INSERT INTO comments (texte, date) VALUES("texte", "04-05-2022"),
("Ceci est un commantaire", "05-05-2022"),
("Encore un commantaire", "06-05-2022"),
("Et Encore un commantaire", "07-05-2022");